#!/usr/bin/env python

import sys

#
# This program simply represents the identity function.
#

max_iter = 10

done = True
in_string = ''
final_list = []
iteration = 0

for line in sys.stdin:
    in_string += line
    line = line.strip().split('\t')
    
    if line[0] == 'Iter':
        iteration = int(line[1])
        continue
    
    node_id = line[0].split('NodeId:')[1]
    # Parse the values
    line = line[1].split(',', 2)
    curr_rank = line[0]
    prev_rank = line[1]
    # Final list for sorting and obtaining highest 20 values.
    final_list += [(float(curr_rank), node_id)]
    if (abs(float(curr_rank) - float(prev_rank)) > 0.0001):
        done = False
    
if not done and iteration < max_iter:
    sys.stdout.write(in_string)
    if iteration == 0:
        sys.stdout.write('Iter\t1\n')
    
else:
    output = ''
    final_list.sort(reverse=True)
    for i in range(20):
        output += ('FinalRank:%0.1f\t%s\n' % final_list[i])
    sys.stdout.write(output)

