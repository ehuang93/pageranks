#!/usr/bin/env python
import os
import sys
import subprocess

# Run until we reach 50 iterations or until output file has a FinalRank in it.
command = 'python pagerank_map.py < slashdot.txt | python sort.py | python pagerank_reduce.py | python process_map.py | python sort.py | python process_reduce.py > output.txt'

# Do one iteration outside the loop to get the original 
os.system(command)

for i in range(1, 50):
    if i % 2 == 1:
        os.system('python pagerank_map.py < output.txt | python sort.py | python pagerank_reduce.py | python process_map.py | python sort.py | python process_reduce.py > input.txt')
        f = open('input.txt', 'r')
        
    else:
        os.system('python pagerank_map.py < input.txt | python sort.py | python pagerank_reduce.py | python process_map.py | python sort.py | python process_reduce.py > output.txt')
        f = open('output.txt', 'r')

    final_list = f.read()
    if 'FinalRank' in final_list:
        print "final rank found"
        break
    f.close()