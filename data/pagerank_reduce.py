#!/usr/bin/env python

import sys

#
# This program simply represents the identity function.
#

alpha = 0.85
prev_key = ''

for line in sys.stdin:
    line = line.strip().split('\t')
    key, value = line[0], line[1]
    
    # Write the iteration directly into the file.
    if key == 'Iter':
        sys.stdout.write(key + '\t' + value + '\n')
        continue    
    
    if key != prev_key:
        
        # Write the appropriate output, given that this is not the first line
        if prev_key != '':
            # Write the appropriate line into the output file
            summed_rank = alpha * summed_rank + 1 - alpha
            sys.stdout.write('NodeId:' + node_id + '\t' + str(summed_rank) + ',' + prev_rank + ',' + neighbors + '\n')
        
        # Set up variables for the next key
        node_id = key
        prev_key = key
        summed_rank = 0.
        
    # Parse the value on this line
    kind, value = value[0], value[1:]
    
    if kind == 'p':
        prev_rank = value
        
    elif kind == 'n':
        # Change delimiters back to ','
        neighbors = value.replace('|', ',')
        
    else:
        # Add the rank distribution to the running sum
        value = value.split('/')
        num, den = value[0], value[1]
        summed_rank += float(num) / float(den)
        
# Do the last write outside the loop
summed_rank = alpha * summed_rank + 1 - alpha
sys.stdout.write('NodeId:' + node_id + '\t' + str(summed_rank) + ',' + prev_rank + ',' + neighbors)
    