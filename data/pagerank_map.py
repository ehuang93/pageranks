#!/usr/bin/env python

import sys

#
# Takes in the file input, and intermediately emits a tag of r, n, or p,
# representing current rank, list of neighbors, and previous rank, respectively.    
#

for line in sys.stdin:
    line = line.strip().split('\t')
    
    # Increment the iteration counter
    if line[0] == 'Iter':
        sys.stdout.write('Iter\t%i\n' % (int(line[1]) + 1))
        continue
    
    node_id = line[0].split('NodeId:')[1] # Simply the node number.
    
    # Parse the values
    line = line[1].split(',', 2)
    rank = line[0] # Current rank.
    
    # Track the previous rank
    sys.stdout.write(node_id + '\t' + 'p' + rank + '\n')
    
    # Track the neighbors
    if len(line) == 2:
        # Has no neighbors; add an edge to itself
        neighbors = node_id
    else:
        # The collector will delimit values by commas, so delimit neighbors
        # by something else...
        neighbors = line[2].replace(',', '|')
        
    sys.stdout.write(node_id + '\t' + 'n' + neighbors + '\n')
    
    # Distribute rank equally to each neighbor
    neighbors = neighbors.split('|')
    degree = str(len(neighbors))
    for neighbor in neighbors:
        sys.stdout.write(neighbor + '\t' + 'r' + rank + '/' + degree + '\n')