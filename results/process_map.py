#!/usr/bin/env python

import sys
from string import split

#
# This program simply represents the identity function.
#

sorted_data = {}
for line in sys.stdin:
    line = split(line, '\t')
    key = line[0]
    value = line[1].strip()
    if key in sorted_data:
        sorted_data[key] += ',' + value
    else:
        sorted_data[key] = value
        
if 'iter' not in sorted_data:
    sys.stdout.write('iter\t2\n')
else:
    sys.stdout.write('iter\t' + sorted_data['iter'] + '\n')
        
l = (len(sorted_data)-1) / 3
skipped = 0
for i in range(l):
    nodeId = 'NodeId:%i' % i
    if 'r %i' % i in sorted_data:
        rank = sorted_data['r %i' % i]
    else:
        rank = '0.0'
        skipped += 1
        
    prev_rank = sorted_data['p %i' % i]
    neighbors = sorted_data['n %i' % i]
    if neighbors:
        sys.stdout.write(nodeId + '\t' + rank + ',' + prev_rank + ',' + neighbors + '\n')
    else:
        sys.stdout.write(nodeId + '\t' + rank + ',' + prev_rank + '\n')

nodes_left = skipped / 3 + 1
if skipped:
    for i in range(l, l + nodes_left):
        nodeId = 'NodeId:%i' % i
        if 'r %i' % i in sorted_data:
            rank = sorted_data['r %i' % i]
        else:
            rank = '0.0'
        prev_rank = sorted_data['p %i' % i]
        neighbors = sorted_data['n %i' % i]
        sys.stdout.write(nodeId + '\t' + rank + ',' + prev_rank + ',' + neighbors + '\n')