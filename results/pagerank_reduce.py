#!/usr/bin/env python

import sys

#
# This program simply represents the identity function.
#

for line in sys.stdin:
    # If given an input of ranks, find and return the summed rank
    # Inputs are a string with form "[TYPE] [NODE_ID] [/T] [VALUES]"
    if line[0] == 'r':
        # Parse input and sum the fractions
        tab_index = line.index('\t')
        node = line[:tab_index]
        ranks = line[tab_index + 1:].strip().split(',')
        summed_rank = 0.
        for frac in ranks:
             num = frac[:frac.index('/')]
             den = frac[frac.index('/') + 1:]
             summed_rank += float(num)/float(den)

        # Output string TYPE ID SUMMED_RANK
        sys.stdout.write(str(node) + '\t' + str(summed_rank) + '\n')

    # If not input of ranks, pass it on unchanged
    else:
        sys.stdout.write(line)
