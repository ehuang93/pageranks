#!/usr/bin/env python

import sys
from string import split

#
# This program simply represents the identity function.
#
done = True
in_string = ''
final_list = []

for input in sys.stdin:
    in_string += input
    line = split(input.strip(), '\t')
    if line[0] == 'iter':
        iteration = int(line[1])
        continue
    
    node_id = split(line[0], 'NodeId:')[1]
    # Parse the values
    line = line[1].split(',', 2)
    curr_rank = line[0]
    prev_rank = line[1]
    # Final list for sorting and obtaining highest 20 values.
    final_list += [(float(curr_rank), node_id)]
    if (abs(float(curr_rank) - float(prev_rank)) > 0.000001):
        done = False

if not done and iteration < 20:
    sys.stdout.write(in_string)
else:
    final_list.sort(reverse=True)
    for i in range(20):
        sys.stdout.write('FinalRank:%0.1f\t%s\n' % final_list[i])
