#!/usr/bin/env python

import sys
from string import split

sorted_data = {}
for line in sys.stdin:
    line = split(line, '\t')
    key = line[0]
    value = line[1].strip()
    if key in sorted_data:
        sorted_data[key] += ',' + value
    else:
        sorted_data[key] = value
    
for key in sorted_data:
    sys.stdout.write(key + '\t' + sorted_data[key] + '\n')