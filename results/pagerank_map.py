#!/usr/bin/env python

import sys
from string import split

#
# This program simply represents the identity function.
#

for line in sys.stdin:
    line = split(line.strip(), '\t')
    # First we will create a key-value pair with the node as the key and
    # its neighbors as the value.  This will be needed when the final output
    # is written.
    if line[0] == 'iter':
        sys.stdout.write('iter\t%i\n' % (int(line[1]) + 1))
        continue
        
    node_id = split(line[0], 'NodeId:')[1]
    
    # Parse the values
    line = line[1].split(',', 2)
    rank = line[0]
    
    sys.stdout.write('p ' + node_id + '\t' + rank + '\n')
    #rank = str(int(float(rank)))
    
    if (len(line) > 2):
        neighbors = line[2]
        sys.stdout.write('n ' + node_id + '\t' + neighbors + '\n')
        neighbors = split(neighbors, ',')
        degree = str(len(neighbors))
        for neighbor in neighbors:
            sys.stdout.write('r ' + neighbor + '\t' + rank + '/' + degree + '\n')
    else: sys.stdout.write('n ' + node_id + '\t \n')
